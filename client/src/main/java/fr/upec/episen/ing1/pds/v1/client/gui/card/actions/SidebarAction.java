package fr.upec.episen.ing1.pds.v1.client.gui.card.actions;

import fr.upec.episen.ing1.pds.v1.client.gui.AfterLogin;
import fr.upec.episen.ing1.pds.v1.client.gui.card.*;
import fr.upec.episen.ing1.pds.v1.client.socket.card.company.Company;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SidebarAction implements ActionListener {
    private final AfterLogin panel;

    public SidebarAction(AfterLogin panel) {
        this.panel = panel;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        panel.dispose();
        String actionCommand = e.getActionCommand();
        Company currentCompany = panel.getCurrentCompany();
        if(actionCommand.equals("ADD_CARD")) {
            new AddCard(currentCompany);
        } else if (actionCommand.equals("MODIFY_CARD")) {
            new ModifyCard(currentCompany);
        } else if(actionCommand.equals("DELETE_CARD")) {
            new DeleteCard(currentCompany);
        } else if(actionCommand.equals("SEARCH_CARD")) {
            new SearchCard(currentCompany);
        } else if(actionCommand.equals("TEST_CARD")) {
            new TestCard(currentCompany);
        }
    }
}
