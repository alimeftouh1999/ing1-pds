package fr.upec.episen.ing1.pds.v1.client.gui.card;

import fr.upec.episen.ing1.pds.v1.client.socket.card.PermissionSocket;
import fr.upec.episen.ing1.pds.v1.client.socket.card.badge.Badge;
import fr.upec.episen.ing1.pds.v1.client.socket.card.company.Company;
import fr.upec.episen.ing1.pds.v1.client.socket.card.person.Person;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ItemListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BadgeAccessList extends JDialog {

    public BadgeAccessList(Badge b, Company currentCompany, JFrame frame) {
        super(frame);
        Map<String, Long> badgeCompany = new HashMap<>();
        badgeCompany.put("'company_id'", currentCompany.getId());
        badgeCompany.put("badge_id", b.getId());

        Map<String, List> data = PermissionSocket.getListOfItems(badgeCompany);
        List<Map> buildings = data.get("buildings");
        List<Map> floors = data.get("floors");
        List<Map> spaces = data.getOrDefault("spaces", List.of());
        JComboBox comboBuilding = new JComboBox();
        for (Map building: buildings) comboBuilding.addItem(building.get("building_name"));

        JComboBox comboFloor = new JComboBox();
        for (Map floor: floors) comboFloor.addItem(String.format("%s - %s", floor.get("building_name"), floor.get("floor_number")));

        JComboBox comboSpace = new JComboBox();
        for (Map space: spaces) comboSpace.addItem(space.get("space_name"));
        JPanel p = new JPanel();
        p.add(comboBuilding);
        p.add(comboFloor);
        p.add(comboSpace);
        this.setContentPane(p);

        this.setSize(400, 800);
        this.setVisible(true);
        this.setLocationRelativeTo(null);
        this.pack();
    }
}
