package fr.upec.episen.ing1.pds.v1.client.gui;

import fr.upec.episen.ing1.pds.v1.client.socket.card.company.Company;
import fr.upec.episen.ing1.pds.v1.client.socket.card.person.PersonSocket;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UiUtils {
    public static JComboBox<Map<String, Object>> getMapJComboBox(Company company) {
        final JComboBox<Map<String, Object>> comboBox;
        comboBox = new JComboBox<>();
        List<Map<String, Object>> personList = PersonSocket.allPersons(company.getId());
        for (Map<String, Object> p: personList) {
            if(p.get("badge") instanceof Map s) {
                String msg = String.format("%s %s - %s", p.get("firstName"), p.get("lastName"), s.get("serialNumber"));
                Map<String, Object> data = new HashMap<>();
                data.put("id", p.get("id"));
                data.put("msg", msg);
                comboBox.addItem(data);
            }
        }

        comboBox.setRenderer(new DefaultListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                if(value instanceof Map m) {
                    setText(m.get("msg").toString());
                }
                return this;
            }
        });
        return comboBox;
    }

}
