package fr.upec.episen.ing1.pds.v1.client.socket.card.building;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import fr.upec.episen.ing1.pds.v1.client.socket.SocketUtil;
import fr.upec.episen.ing1.pds.v1.client.socket.card.company.Company;
import fr.upec.episen.ing1.pds.v1.client.socket.serialization.Request;
import fr.upec.episen.ing1.pds.v1.client.socket.serialization.Response;

import java.util.List;
import java.util.Map;

public class BuildSocket {
    private final static ObjectMapper MAPPER = new ObjectMapper();
    public static List<Map<String, Object>> getBuildingList(Company company) {
        Request request = new Request("BUILDING_LIST");
        ObjectNode data = MAPPER.createObjectNode();
        data.put("'company_id'", company.getId());
        request.setData(data);
        Response<List<Map<String, Object>>> response =  SocketUtil.sendRequest(request);
        return response.getMessage();
    }

    public static Map<String, Object> getBuilding(Long id) {
        Request request = new Request("BUILDING_BY_ID");
        ObjectNode data = MAPPER.createObjectNode();
        data.put("'id'", id);
        request.setData(data);
        Response<Map<String, Object>> response =  SocketUtil.sendRequest(request);
        return response.getMessage();
    }

    public static List<Map<String, Object>> getFloorListByBuilding(Long id) {
        Request request = new Request("FLOOR_BY_BUILDING");
        ObjectNode data = MAPPER.createObjectNode();
        data.put("'building_id'", id);
        request.setData(data);
        Response<List<Map<String, Object>>> response =  SocketUtil.sendRequest(request);
        return response.getMessage();
    }
    public static List<Map<String, Object>> getSpaceListByFloor(Long id) {
        Request request = new Request("SPACES_BY_FLOOR");
        ObjectNode data = MAPPER.createObjectNode();
        data.put("'floor_id'", id);
        request.setData(data);
        Response<List<Map<String, Object>>> response =  SocketUtil.sendRequest(request);
        return response.getMessage();
    }



}
