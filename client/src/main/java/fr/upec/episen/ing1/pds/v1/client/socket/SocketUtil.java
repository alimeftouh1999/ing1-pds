package fr.upec.episen.ing1.pds.v1.client.socket;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import fr.upec.episen.ing1.pds.v1.client.socket.serialization.Request;
import fr.upec.episen.ing1.pds.v1.client.socket.serialization.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class SocketUtil {
    private static final SocketSG socketSG = SocketSG.getInstance();
    private final static ObjectMapper MAPPER = new ObjectMapper();
    private final static Logger LOGGER = LoggerFactory.getLogger(SocketUtil.class);

    public static <T> Response<T> sendRequest(Request request) {
       try {
           Socket socket = socketSG.getSocket();
           PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
           BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
           String msgToSend = MAPPER.writeValueAsString(request);
           LOGGER.info("We're sending: {}", msgToSend);
           writer.println(msgToSend);
           String msg = reader.readLine();
           return  MAPPER.readValue(msg, new TypeReference<>() {});
       } catch (Exception e) {
           e.printStackTrace();
           throw new RuntimeException(e.getMessage());
       }
    }
}
