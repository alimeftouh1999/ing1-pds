package fr.upec.episen.ing1.pds.v1.client.gui;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.upec.episen.ing1.pds.v1.client.socket.card.company.Company;
import fr.upec.episen.ing1.pds.v1.client.socket.card.company.CompanySocket;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;
import java.util.Vector;

public class HomePage extends JFrame implements ActionListener {
    private final ObjectMapper MAPPER = new ObjectMapper();
    private JButton button;
    private JComboBox<Company> comboBox;
    public HomePage()  {
        this.setSize(800, 500);
        this.setPreferredSize(this.getSize());

        JPanel panel = new JPanel(new GridLayout(3, 1, 40, 40));
        JLabel header = new JLabel("Choisissez votre entreprise", SwingConstants.CENTER);
        header.setFont(new Font("ROBOTO", Font.BOLD, 18));
        panel.add(header);

        comboBox = new JComboBox<>(new Vector<>(CompanySocket.getAllCompanyList()));

        comboBox.setRenderer(new DefaultListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                if(value instanceof Map) {
                    setText(((Map) value).get("name").toString());
                }
                return this;
            }
        });

        panel.add(comboBox);

        button = new JButton("Valider");
        button.addActionListener(this);
        panel.add(button);

        this.setContentPane(panel);
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton eBtn = (JButton) e.getSource();
        if(eBtn == button) {
            this.dispose();
            Company company = MAPPER.convertValue(comboBox.getSelectedItem(), Company.class);
            new AfterLogin(company);
        }
    }
}
