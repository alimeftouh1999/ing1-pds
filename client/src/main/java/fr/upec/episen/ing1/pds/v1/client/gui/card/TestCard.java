package fr.upec.episen.ing1.pds.v1.client.gui.card;

import fr.upec.episen.ing1.pds.v1.client.gui.UiUtils;
import fr.upec.episen.ing1.pds.v1.client.socket.card.PermissionSocket;
import fr.upec.episen.ing1.pds.v1.client.socket.card.badge.Badge;
import fr.upec.episen.ing1.pds.v1.client.socket.card.building.BuildSocket;
import fr.upec.episen.ing1.pds.v1.client.socket.card.company.Company;
import fr.upec.episen.ing1.pds.v1.client.socket.card.person.Person;
import fr.upec.episen.ing1.pds.v1.client.socket.card.person.PersonSocket;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Map;
import java.util.Objects;

public class TestCard extends CardHome implements ItemListener {
    private final JComboBox<Map<String, Object>> comboBox;
    private final JComboBox<Map<String, Object>> buildings, floors, spaces;
    private final JButton testBtn;
    public TestCard(Company company) {
        super(company);
        context.add(new JLabel(""));
        context.setLayout(new BorderLayout(40, 40));
        comboBox = UiUtils.getMapJComboBox(company);
        JPanel form = new JPanel(new BorderLayout(40, 40));
        form.add(new JLabel("Choisissez un utilisateur"), BorderLayout.WEST);
        form.add(comboBox, BorderLayout.CENTER);
        form.setBorder(new EmptyBorder(20, 5, 5, 5));
        context.add(form, BorderLayout.NORTH);
        buildings = new JComboBox<>();
        buildings.setBorder(BorderFactory.createTitledBorder("Les Batiments"));

        floors = new JComboBox<>();
        floors.addItemListener(this);
        floors.setBorder(BorderFactory.createTitledBorder("Les Étages"));

        spaces = new JComboBox<>();
        spaces.setBorder(BorderFactory.createTitledBorder("Les Espaces"));

        JPanel items = new JPanel(new GridLayout(1, 3, 60, 60));
        items.add(buildings); items.add(floors); items.add(spaces);

        JPanel container = new JPanel(new GridLayout(10, 1));
        container.add(items);

        context.add(container);

        JPanel btnP = new JPanel();
        testBtn = new JButton("Tester");
        testBtn.addActionListener(this);
        btnP.add(testBtn);

        context.add(btnP, BorderLayout.SOUTH);
        handleBuildingComboBox();
    }

    private void handleBuildingComboBox() {
        buildings.addItemListener(this);
        buildings.removeAllItems();
        for (Map<String, Object> building: BuildSocket.getBuildingList(currentCompany)) {
            buildings.addItem(building);
        }
        buildings.setRenderer(new DefaultListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                if(index == -1 && value == null) {
                    setText("Choisissez le batiement voulu");
                } else if(value instanceof Map m) {
                    setText(m.get("building_name").toString());
                }
                return this;
            }
        });
        buildings.setSelectedIndex(-1);
    }
    private void setupComboBoxFloors(Map<String, Object> b) {
        floors.removeAllItems();
        Long id = Long.valueOf(String.valueOf(b.get("id")));
        for (Map<String, Object> floor : BuildSocket.getFloorListByBuilding(id)) {
            floors.addItem(floor);
        }
        floors.setSelectedIndex(-1);
        floors.setRenderer(new DefaultListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                if(index == -1 && value == null) {
                    setText("Choisissez l'étage voulu");
                } else if(value instanceof Map m) {
                    setText(b.get("building_name")+ " " + m.get("floor_number").toString());
                }
                return this;
            }
        });
    }

    private void setupComboBoxSpaces(Map<String, Object> f) {
        spaces.removeAllItems();
        Long id = Long.valueOf(String.valueOf(f.get("floor_id")));
        for (Map<String, Object> space : BuildSocket.getSpaceListByFloor(id)) {
            spaces.addItem(space);
        }
        spaces.setSelectedIndex(-1);
        spaces.setRenderer(new DefaultListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                if(index == -1 && value == null) {
                    setText("Choisissez l'espace voulu");
                } else if(value instanceof Map m) {
                    setText(m.get("space_name").toString());
                }
                return this;
            }
        });
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        if(e.getStateChange() == e.getStateChange()) {
            if(Objects.equals(e.getSource(), buildings)) {
                if(buildings.getSelectedIndex() > -1) {
                    setupComboBoxFloors((Map<String, Object>) buildings.getSelectedItem());
                }
            }
            if(Objects.equals(e.getSource(), floors)) {
                if(floors.getSelectedIndex() > -1) {
                    setupComboBoxSpaces((Map<String, Object>) floors.getSelectedItem());
                }
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        super.actionPerformed(e);
        if(Objects.equals(e.getSource(), testBtn)) {
            Map<String, Object> data = (Map<String, Object>) comboBox.getSelectedItem();
            assert data != null;
            Long id = Long.valueOf(String.valueOf(data.get("id")));
            Person p = PersonSocket.getPersonById(id);
            Badge badge = p.getBadge();
            if(buildings.getSelectedIndex() > -1) {
                if(floors.getSelectedIndex() > -1) {
                    if(spaces.getSelectedIndex() > -1) {
                        Map<String, Object> space = (Map<String, Object>) spaces.getSelectedItem();
                        assert space != null;
                        Long spaceId = Long.valueOf(String.valueOf(space.get("space_id")));
                        Boolean hasAccess = PermissionSocket.hasAccessDesk(Map.of("space_id", spaceId, "badge_id", badge.getId()));
                        if(hasAccess) {
                            JOptionPane.showMessageDialog(
                                    null,
                                    "Vous avez l'accès",
                                    "Success",
                                    JOptionPane.INFORMATION_MESSAGE
                            );
                        } else  {
                            JOptionPane.showMessageDialog(null,
                                    "Vous n'avez pas l'accès", "Refus",
                                    JOptionPane.ERROR_MESSAGE);
                        }
                    } else {
                        Map<String, Object> floor = (Map<String, Object>) floors.getSelectedItem();
                        assert floor != null;
                        Long floorId = Long.valueOf(String.valueOf(floor.get("floor_id")));
                        Boolean hasAccess = PermissionSocket.hasAccessFloor(Map.of("floor_id", floorId, "badge_id", badge.getId()));
                        if(hasAccess) {
                            JOptionPane.showMessageDialog(
                                    null,
                                    "Vous avez l'accès",
                                    "Success",
                                    JOptionPane.INFORMATION_MESSAGE
                            );
                        } else  {
                            JOptionPane.showMessageDialog(null,
                                    "Vous n'avez pas l'accès", "Refus",
                                    JOptionPane.ERROR_MESSAGE);
                        }
                    }
                } else {
                    Map<String, Object> building = (Map<String, Object>) buildings.getSelectedItem();
                    assert building != null;
                    Long buildingId = Long.valueOf(String.valueOf(building.get("id")));
                    Boolean hasAccess = PermissionSocket.hasAccessBuilding(Map.of("building_id", buildingId, "badge_id", badge.getId()));
                    if(hasAccess) {
                        JOptionPane.showMessageDialog(
                                null,
                                "Vous avez l'accès",
                                "Success",
                                JOptionPane.INFORMATION_MESSAGE
                        );
                    } else  {
                        JOptionPane.showMessageDialog(null,
                                "Vous n'avez pas l'accès", "Refus",
                                JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        }
    }
}
