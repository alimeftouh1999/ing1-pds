package fr.upec.episen.ing1.pds.v1.client;

import fr.upec.episen.ing1.pds.v1.client.gui.HomePage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Client {
    private final static Logger LOGGER = LoggerFactory.getLogger(Client.class);
    public static void main(String[] args) {
        LOGGER.info("Client Started !");
        new HomePage();
    }
}
