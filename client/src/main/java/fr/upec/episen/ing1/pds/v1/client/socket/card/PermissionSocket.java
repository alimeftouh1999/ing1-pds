package fr.upec.episen.ing1.pds.v1.client.socket.card;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.upec.episen.ing1.pds.v1.client.socket.SocketUtil;
import fr.upec.episen.ing1.pds.v1.client.socket.serialization.Request;
import fr.upec.episen.ing1.pds.v1.client.socket.serialization.Response;

import java.util.List;
import java.util.Map;

public class PermissionSocket {
    private final static ObjectMapper MAPPER = new ObjectMapper();

    public static void grantAccessByDesk(Map<String, Long> spaceBadge) {
        JsonNode data =  MAPPER.valueToTree(spaceBadge);
        Request request = new Request("GRANT_ACCESS_DESK");
        request.setData(data);
        SocketUtil.sendRequest(request);
    }
    public static void pullOffBySpace(Map<String, Long> spaceBadge) {
        JsonNode data =  MAPPER.valueToTree(spaceBadge);
        Request request = new Request("PULL_ACCESS_DESK");
        request.setData(data);
        SocketUtil.sendRequest(request);
    }

    public static void grantAccessByFloor(Map<String, Long> floorBadge) {
        JsonNode data =  MAPPER.valueToTree(floorBadge);
        Request request = new Request("GRANT_ACCESS_FLOOR");
        request.setData(data);
        SocketUtil.sendRequest(request);
    }
    public static void pullOffByFloor(Map<String, Long> floorBadge) {
        JsonNode data =  MAPPER.valueToTree(floorBadge);
        Request request = new Request("PULL_ACCESS_FLOOR");
        request.setData(data);
        SocketUtil.sendRequest(request);
    }

    public static void grantAccessByBuilding(Map<String, Long> buildingBadge) {
        JsonNode data =  MAPPER.valueToTree(buildingBadge);
        Request request = new Request("GRANT_ACCESS_BUILDING");
        request.setData(data);
        SocketUtil.sendRequest(request);
    }
    public static void pullOffByBuilding(Map<String, Long> buildingBadge) {
        JsonNode data =  MAPPER.valueToTree(buildingBadge);
        Request request = new Request("PULL_ACCESS_BUILDING");
        request.setData(data);
        SocketUtil.sendRequest(request);
    }

    public static Boolean hasAccessBuilding(Map<String, Long> badge) {
        JsonNode data =  MAPPER.valueToTree(badge);
        Request request = new Request("HAS_ACCESS_BUILDING");
        request.setData(data);
        Response<Boolean> response = SocketUtil.sendRequest(request);
        return response.getMessage();
    }
    public static Boolean hasAccessFloor(Map<String, Long> badge) {
        JsonNode data =  MAPPER.valueToTree(badge);
        Request request = new Request("HAS_ACCESS_FLOOR");
        request.setData(data);
        Response<Boolean> response = SocketUtil.sendRequest(request);
        return response.getMessage();
    }
    public static Boolean hasAccessDesk(Map<String, Long> badge) {
        JsonNode data =  MAPPER.valueToTree(badge);
        Request request = new Request("HAS_ACCESS_SPACE");
        request.setData(data);
        Response<Boolean> response = SocketUtil.sendRequest(request);
        return response.getMessage();
    }

    public static Map<String, List> getListOfItems(Map<String, Long> badgeCompany)  {
        JsonNode data =  MAPPER.valueToTree(badgeCompany);
        Request request = new Request("ACCESS_ITEMS");
        request.setData(data);
        Response<Map<String, List>> response = SocketUtil.sendRequest(request);
        return response.getMessage();
    }
}
