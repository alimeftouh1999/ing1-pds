package fr.upec.episen.ing1.pds.v1.client.socket.card.company;

public class Company {
    private Long id;
    private String name;
    private String address;

    public Company() {

    }
    public Company(Long id, String name, String address) {
        this.id = id;
        this.name = name;
        this.address = address;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
