package fr.upec.episen.ing1.pds.v1.client.socket.card.person;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import fr.upec.episen.ing1.pds.v1.client.socket.SocketUtil;
import fr.upec.episen.ing1.pds.v1.client.socket.card.badge.Badge;
import fr.upec.episen.ing1.pds.v1.client.socket.serialization.Request;
import fr.upec.episen.ing1.pds.v1.client.socket.serialization.Response;

import java.util.List;
import java.util.Map;

public class PersonSocket {
    private final static ObjectMapper MAPPER = new ObjectMapper();

    public static void savePerson (Person person) {
        Request request = new Request("SAVE_PERSON");
        request.setData(MAPPER.convertValue(person, JsonNode.class));
        SocketUtil.sendRequest(request);
    }

    public static List<Map<String, Object>> allPersons(Long companyId) {
        Request request = new Request("PERSONS_LIST");
        ObjectNode data = MAPPER.createObjectNode();
        data.put("'company_id'", companyId);
        request.setData(data);
        Response<List<Map<String, Object>>> response =  SocketUtil.sendRequest(request);
        return response.getMessage();
    }
    public static void deletePerson(Person p) {
        Request request = new Request("DELETE_PERSON");
        request.setData(MAPPER.convertValue(p, JsonNode.class));
        SocketUtil.sendRequest(request);
    }

    public static Person getPersonById (Long id) {
        Request request = new Request("PERSON_BY_ID");
        ObjectNode data = MAPPER.createObjectNode();
        data.put("'person_id'", id);
        request.setData(data);
        Response<Person> response =  SocketUtil.sendRequest(request);
        return MAPPER.convertValue(response.getMessage(), Person.class);
    }

    public static Person updatePerson (Long id, Person p)  {
        Request request = new Request("UPDATE_PERSON");
        ObjectNode data = MAPPER.createObjectNode();
        data.put("'person_id'", id);
        try {
            data.put("person_data", MAPPER.writeValueAsString(p));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        request.setData(data);
        Response<Person> response =  SocketUtil.sendRequest(request);
        return MAPPER.convertValue(response.getMessage(), Person.class);
    }

}
