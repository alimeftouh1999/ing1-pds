package fr.upec.episen.ing1.pds.v1.client.gui.card;

import fr.upec.episen.ing1.pds.v1.client.socket.card.company.Company;
import fr.upec.episen.ing1.pds.v1.client.socket.card.person.Person;
import fr.upec.episen.ing1.pds.v1.client.socket.card.person.PersonSocket;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class DeleteCard extends CardHome {
    private final JButton click;
    private final JComboBox<Map<Object, String>> comboBox;
    public DeleteCard(Company company) {
        super(company);
        context.add(new JLabel(""));
        context.setLayout(new FlowLayout());
        comboBox = new JComboBox<>();
        List<Map<String, Object>> personList = PersonSocket.allPersons(company.getId());
        for (Map<String, Object> p: personList) {
            if(p.get("badge") instanceof Map s) {
                String msg = String.format("%s %s - %s", p.get("firstName"), p.get("lastName"), s.get("serialNumber"));
                comboBox.addItem(Map.of(p.get("id"), msg));
            }
        }
        comboBox.setRenderer(new DefaultListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                if(value instanceof Map m) {
                    Iterator msg = m.values().iterator();
                    if(msg.hasNext()) setText(msg.next().toString());
                }
                return this;
            }
        });
        JPanel form = new JPanel(new BorderLayout(40, 40));
        form.add(new JLabel("Choisissez un utilisateur"), BorderLayout.WEST);
        form.add(comboBox, BorderLayout.CENTER);
        click = new JButton("Supprimer");
        click.addActionListener(this);
        form.add(click, BorderLayout.EAST);
        form.setBorder(new EmptyBorder(20, 5, 5, 5));
        context.add(form, FlowLayout.CENTER);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        super.actionPerformed(e);
        if(click == e.getSource()) {
            Map<Object, String> selected = (Map<Object, String>) comboBox.getSelectedItem();
            assert selected != null;
            Iterator<Object> ids = selected.keySet().iterator();
            if(ids.hasNext()) {
                Long id = Long.valueOf("" + ids.next());
                Person p = new Person();
                p.setId(id);
                PersonSocket.deletePerson(p);
                this.dispose();
                new DeleteCard(this.currentCompany);
                JOptionPane.showMessageDialog(
                        null,
                        "Supprimer avec succès",
                        "Success",
                        JOptionPane.INFORMATION_MESSAGE
                );
            }
        }
    }
}
