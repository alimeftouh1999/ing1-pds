package fr.upec.episen.ing1.pds.v1.client.socket.card.company;

import fr.upec.episen.ing1.pds.v1.client.socket.serialization.Request;
import fr.upec.episen.ing1.pds.v1.client.socket.serialization.Response;
import fr.upec.episen.ing1.pds.v1.client.socket.SocketUtil;

import java.util.List;

public class CompanySocket {
    public static List<Company> getAllCompanyList() {
        Request request = new Request("COMPANY_LIST");
        Response response =  SocketUtil.sendRequest(request);
        return (List<Company>) response.getMessage();
    }
}
