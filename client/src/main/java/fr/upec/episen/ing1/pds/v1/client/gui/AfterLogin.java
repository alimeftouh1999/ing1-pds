package fr.upec.episen.ing1.pds.v1.client.gui;

import fr.upec.episen.ing1.pds.v1.client.gui.card.CardHome;
import fr.upec.episen.ing1.pds.v1.client.socket.card.company.Company;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AfterLogin extends JFrame implements ActionListener {
    protected JPanel content = new JPanel(new GridLayout(3, 1));
    protected final Company currentCompany;
    protected JButton configBtn;
    protected JButton discBtn;
    public AfterLogin(Company currentCompany) {
        this.currentCompany = currentCompany;
        this.setSize(1200, 800);
        this.setPreferredSize(this.getSize());

        JPanel panel = new JPanel(new BorderLayout());

        JPanel header = new JPanel(new BorderLayout());
        discBtn = new JButton("Se déconnecter");
        discBtn.addActionListener(this);
        header.setBackground(Color.GRAY);
        header.add(discBtn, BorderLayout.EAST);
        JLabel companyLabel = new JLabel(currentCompany.getName());
        companyLabel.setForeground(Color.WHITE);
        header.add(companyLabel, BorderLayout.WEST);
        panel.add(header, BorderLayout.NORTH);

        configBtn = new JButton("Configuration des badges");
        configBtn.addActionListener(this);

        content.add(new JLabel());
        content.add(configBtn);
        content.add(new JLabel());
        panel.add(content, BorderLayout.CENTER);
        this.setContentPane(panel);
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton eBtn = (JButton) e.getSource();
        if(eBtn == configBtn) {
            this.dispose();
            new CardHome(currentCompany);
        } else if(eBtn == discBtn) {
            this.dispose();
            new HomePage();
        }
    }

    public Company getCurrentCompany() {
        return currentCompany;
    }
}
