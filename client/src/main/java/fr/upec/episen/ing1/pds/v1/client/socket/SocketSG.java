package fr.upec.episen.ing1.pds.v1.client.socket;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

public class SocketSG {
    private final Socket socket;
    private static SocketSG instance;

    private SocketSG() throws IOException {
        this.socket = new Socket(InetAddress.getByName("172.31.250.2"), 61100);
    }

    protected static SocketSG getInstance() {
        if(instance == null) {
            try {
                instance = new SocketSG();
            } catch (IOException e) {
                e.printStackTrace();
                throw new RuntimeException(e.getCause());
            }
        }
        return instance;
    }

    protected Socket getSocket() {
        return socket;
    }
}
