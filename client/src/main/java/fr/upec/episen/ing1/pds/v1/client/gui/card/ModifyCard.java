package fr.upec.episen.ing1.pds.v1.client.gui.card;

import fr.upec.episen.ing1.pds.v1.client.gui.UiUtils;
import fr.upec.episen.ing1.pds.v1.client.socket.card.PermissionSocket;
import fr.upec.episen.ing1.pds.v1.client.socket.card.badge.Badge;
import fr.upec.episen.ing1.pds.v1.client.socket.card.building.BuildSocket;
import fr.upec.episen.ing1.pds.v1.client.socket.card.company.Company;
import fr.upec.episen.ing1.pds.v1.client.socket.card.person.Person;
import fr.upec.episen.ing1.pds.v1.client.socket.card.person.PersonSocket;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class ModifyCard extends CardHome implements ItemListener {
    private final JButton click;
    private JButton update, pullOff, giveBtn, infoAccessBtn;
    private JTextField fName, lName, activationDate;
    private JPanel editPanel, permissionPanel;
    private JCheckBox active;
    private JComboBox<String> status;
    private JComboBox<Map<String, Object>> buildings, floors, spaces;
    private final JComboBox<Map<String, Object>> comboBox;
    private final Map<String, Person> personSelected = new HashMap<>();

    public ModifyCard(Company company) {
        super(company);
        context.removeAll();

        editPanel = new JPanel();
        editPanel.setVisible(false);

        context.add(new JLabel(""));
        context.setLayout(new BorderLayout(40, 40));
        comboBox = UiUtils.getMapJComboBox(company);
        JPanel form = new JPanel(new BorderLayout(40, 40));
        form.add(new JLabel("Choisissez un utilisateur"), BorderLayout.WEST);
        form.add(comboBox, BorderLayout.CENTER);
        click = new JButton("Choisir");
        click.addActionListener(this);
        form.add(click, BorderLayout.EAST);
        form.setBorder(new EmptyBorder(20, 5, 5, 5));
        context.add(form, BorderLayout.NORTH);
        context.add(editPanel, BorderLayout.CENTER);
    }


    private void editPanelInit(Long id) {
        Person p = PersonSocket.getPersonById(id);
        personSelected.put("p", p);
        editPanel.setLayout(new GridLayout(4, 1));
        JPanel personPanel = new JPanel();
        personPanel.setBorder(BorderFactory.createTitledBorder("Information Utilisateur"));
        fName = new JTextField(p.getFirstName(), 20);
        lName = new JTextField(p.getLastName(), 20);
        personPanel.add(fName);
        personPanel.add(lName);

        // Status active
        editPanel.add(personPanel);
        JPanel cardPanel = new JPanel();
        Badge b = p.getBadge();
        cardPanel.setBorder(BorderFactory.createTitledBorder("Information Badge"));
        // status profile
        status = new JComboBox<>();
        status.addItem("Administrateur");
        status.addItem("Employé");
        status.addItem("Visiteur");
        status.setSelectedItem(b.getType());
        status.addItemListener(this);

        cardPanel.add(status);
        // Status active
        active = new JCheckBox("Actif ?");
        active.setSelected(b.getActive());
        // Date Activation
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        activationDate = new JFormattedTextField(dateFormat);
        activationDate.setColumns(20);
        if(Objects.nonNull(b.getEndDate())) {
            activationDate.setText(dateFormat.format(b.getEndDate()));
        }
        activationDate.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
            }
            @Override
            public void focusLost(FocusEvent e) {
                try{
                    dateFormat.parse(activationDate.getText());
                    update.setEnabled(true);
                    update.setDefaultCapable(true);
                    update.removeAll();
                } catch(Exception ext){
                    update.setEnabled(false);
                    update.setDefaultCapable(false);
                    update.removeAll();
                    JOptionPane.showMessageDialog(
                            null,
                            "Erreur",
                            "Error",
                            JOptionPane.ERROR_MESSAGE
                    );
                    update.setEnabled(true);
                    update.setDefaultCapable(true);
                    update.removeAll();
                }
            }
        });

        JLabel label = new JLabel("Date d'expiration");
        label.setLabelFor(activationDate);
        cardPanel.add(label);
        cardPanel.add(activationDate);
        cardPanel.add(active);

        editPanel.add(cardPanel);

        permissionPanel = new JPanel(new GridLayout(2, 1, 40, 40));
        permissionPanel.setBorder(BorderFactory.createTitledBorder("Information Permission"));

        JPanel comboBoxes = new JPanel(new GridLayout(1, 3, 80, 80));
        JPanel btns = new JPanel();

        buildings = new JComboBox<>();
        buildings.setBorder(BorderFactory.createTitledBorder("Les Batiments"));
        comboBoxes.add(buildings);

        floors = new JComboBox<>();
        floors.setBorder(BorderFactory.createTitledBorder("Les Étages"));
        comboBoxes.add(floors);

        spaces = new JComboBox<>();
        spaces.setBorder(BorderFactory.createTitledBorder("Les Espaces"));
        comboBoxes.add(spaces);

        buildings.addItemListener(this);
        floors.addItemListener(this);
        spaces.addItemListener(this);

        floors.setEnabled(false);
        spaces.setEnabled(false);

        pullOff = new JButton("Retirer");
        giveBtn = new JButton("Attribuer");
        infoAccessBtn = new JButton("Information Accès");

        giveBtn.addActionListener(this);
        pullOff.addActionListener(this);
        infoAccessBtn.addActionListener(this);
        btns.add(giveBtn);
        btns.add(pullOff);
        btns.add(infoAccessBtn);

        permissionPanel.add(comboBoxes);
        permissionPanel.add(btns);

        update = new JButton("Sauvegarder");
        update.addActionListener(this);

        JPanel saveUpdate = new JPanel(new FlowLayout(FlowLayout.CENTER));
        saveUpdate.add(update);
        editPanel.add(saveUpdate);
        editPanel.add(permissionPanel);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        super.actionPerformed(e);
        JButton eSource = (JButton) e.getSource();
        Person p;
        Badge b = null;

        if(eSource == click) {
            Map<String, Object> data = (Map<String, Object>) comboBox.getSelectedItem();
            assert data != null;
            Long id = Long.valueOf(String.valueOf(data.get("id")));
            editPanel.removeAll();
            editPanelInit(id);
            displayPermissionSettingsForNoneAdmin();
            editPanel.setVisible(true);
            editPanel.invalidate();
            editPanel.repaint();
            editPanel.revalidate();
        }
        else if(eSource == infoAccessBtn) {
            p = personSelected.get("p");
            b = p.getBadge();
            new BadgeAccessList(b, currentCompany, this);
        }
        else if(eSource == update) {
            p = personSelected.get("p");
            p.setFirstName(fName.getText());
            p.setLastName(lName.getText());
            p.setVisitor(Objects.equals(status.getSelectedItem(), "Visiteur"));
            b = p.getBadge();
            b.setActive(active.isSelected());
            b.setType(Objects.requireNonNull(status.getSelectedItem()).toString());
            Date endDate = (Date) ((JFormattedTextField) activationDate).getValue();
            b.setEndDate(endDate);
            p.setBadge(b);
            PersonSocket.updatePerson(p.getId(), p);
            JOptionPane.showMessageDialog(
                    null,
                    "Enregistrer avec succès",
                    "Success",
                    JOptionPane.INFORMATION_MESSAGE
            );
        }

        else if(Objects.equals(eSource, giveBtn)) {
            Badge badge = personSelected.get("p").getBadge();
            if(buildings.getSelectedIndex() > -1) {
                if(floors.getSelectedIndex() > -1) {
                    if(spaces.getSelectedIndex() > -1) {
                        Map<String, Object> space = (Map<String, Object>) spaces.getSelectedItem();
                        assert space != null;
                        Long spaceId = Long.valueOf(String.valueOf(space.get("space_id")));
                        PermissionSocket.grantAccessByDesk(Map.of("space_id", spaceId, "badge_id", badge.getId()));
                    } else {
                        Map<String, Object> floor = (Map<String, Object>) floors.getSelectedItem();
                        assert floor != null;
                        Long floorId = Long.valueOf(String.valueOf(floor.get("floor_id")));
                        PermissionSocket.grantAccessByFloor(Map.of("floor_id", floorId, "badge_id", badge.getId()));
                    }
                } else {
                    Map<String, Object> building = (Map<String, Object>) buildings.getSelectedItem();
                    assert building != null;
                    Long buildingId = Long.valueOf(String.valueOf(building.get("id")));
                    PermissionSocket.grantAccessByBuilding(Map.of("building_id", buildingId, "badge_id", badge.getId()));
                }
                JOptionPane.showMessageDialog(
                        null,
                        "Attribuer avec succès",
                        "Success",
                        JOptionPane.INFORMATION_MESSAGE
                );
            }
        }
        else if(Objects.equals(eSource, pullOff)) {
            Badge badge = personSelected.get("p").getBadge();
            if(buildings.getSelectedIndex() > -1) {
                if(floors.getSelectedIndex() > -1) {
                    if(spaces.getSelectedIndex() > -1) {
                        Map<String, Object> space = (Map<String, Object>) spaces.getSelectedItem();
                        assert space != null;
                        Long spaceId = Long.valueOf(String.valueOf(space.get("space_id")));
                        PermissionSocket.pullOffBySpace(Map.of("space_id", spaceId, "badge_id", badge.getId()));
                    } else {
                        Map<String, Object> floor = (Map<String, Object>) floors.getSelectedItem();
                        assert floor != null;
                        Long floorId = Long.valueOf(String.valueOf(floor.get("floor_id")));
                        PermissionSocket.pullOffByFloor(Map.of("floor_id", floorId, "badge_id", badge.getId()));
                    }
                } else {
                    Map<String, Object> building = (Map<String, Object>) buildings.getSelectedItem();
                    assert building != null;
                    Long buildingId = Long.valueOf(String.valueOf(building.get("id")));
                    PermissionSocket.pullOffByBuilding(Map.of("building_id", buildingId, "badge_id", badge.getId()));
                }
                JOptionPane.showMessageDialog(
                        null,
                        "Retirer avec succès",
                        "Success",
                        JOptionPane.INFORMATION_MESSAGE
                );
            }
        }
    }

    private void displayPermissionSettingsForNoneAdmin () {
        if(status.getSelectedIndex() == 0) {
            permissionPanel.setVisible(false);
        } else  {
            permissionPanel.setVisible(true);
            handleBuildingComboBoxes();
        }
    }

    private void handleBuildingComboBoxes() {
        buildings.removeAllItems();
        for (Map<String, Object> building: BuildSocket.getBuildingList(currentCompany)) {
            buildings.addItem(building);
        }
        buildings.setRenderer(new DefaultListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                if(index == -1 && value == null) {
                    setText("Choisissez le batiment voulu");
                } else if(value instanceof Map m) {
                    setText(m.get("building_name").toString());
                }
                return this;
            }
        });
        buildings.setSelectedIndex(-1);
    }

    private void setupComboBoxFloors(Map<String, Object> b) {
      floors.removeAllItems();
      Long id = Long.valueOf(String.valueOf(b.get("id")));
      for (Map<String, Object> floor : BuildSocket.getFloorListByBuilding(id)) {
            floors.addItem(floor);
      }
      floors.setSelectedIndex(-1);
      floors.setRenderer(new DefaultListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                if(index == -1 && value == null) {
                    setText("Choisissez l'étage voulu");
                } else if(value instanceof Map m) {
                    setText(b.get("building_name")+ " " + m.get("floor_number").toString());
                }
                return this;
            }
        });
    }

    private void setupComboBoxSpaces(Map<String, Object> f) {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        spaces.removeAllItems();
        Long id = Long.valueOf(String.valueOf(f.get("floor_id")));
        for (Map<String, Object> space : BuildSocket.getSpaceListByFloor(id)) {
            spaces.addItem(space);
        }
        spaces.setSelectedIndex(-1);
        spaces.setRenderer(new DefaultListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                if(index == -1 && value == null) {
                    setText("Choisissez l'espace voulu");
                } else if(value instanceof Map m) {
                    setText(m.get("space_name").toString());
                }
                return this;
            }
        });
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        Object s = e.getSource();
        if(e.getStateChange() == ItemEvent.SELECTED) {
            if(Objects.equals(s, status)) {
                displayPermissionSettingsForNoneAdmin();
            } else if(Objects.equals(s, buildings)) {
                System.out.println(buildings.getSelectedIndex());
                if(buildings.getSelectedIndex() > -1) {
                    floors.setEnabled(true);
                    Map<String, Object> b = (Map<String, Object>) buildings.getSelectedItem();
                    assert b != null;
                    setupComboBoxFloors(b);
                }
                else floors.setEnabled(false);
            } else if(Objects.equals(s, floors)) {
                if(floors.getSelectedIndex() > -1 && floors.isEnabled()) {
                    spaces.setEnabled(true);
                    Map<String, Object> f = (Map<String, Object>) floors.getSelectedItem();
                    assert f != null;
                    setupComboBoxSpaces(f);
                }
                else
                    spaces.setEnabled(false);
            }
        }
    }
}