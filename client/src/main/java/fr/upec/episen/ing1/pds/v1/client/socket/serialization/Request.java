package fr.upec.episen.ing1.pds.v1.client.socket.serialization;

import com.fasterxml.jackson.databind.JsonNode;

public class Request {
    private String event;
    private JsonNode data;

    public Request(String event) {
        this.event = event;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public JsonNode getData() {
        return data;
    }

    public void setData(JsonNode data) {
        this.data = data;
    }
}
