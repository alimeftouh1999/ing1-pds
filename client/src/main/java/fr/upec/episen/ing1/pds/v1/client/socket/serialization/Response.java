package fr.upec.episen.ing1.pds.v1.client.socket.serialization;

public class Response<T> {
    private boolean success;
    private T message;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public T getMessage() {
        return message;
    }

    public void setMessage(T message) {
        this.message = message;
    }
}
