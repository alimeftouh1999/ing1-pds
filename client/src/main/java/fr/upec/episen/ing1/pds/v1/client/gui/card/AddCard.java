package fr.upec.episen.ing1.pds.v1.client.gui.card;

import fr.upec.episen.ing1.pds.v1.client.socket.card.badge.Badge;
import fr.upec.episen.ing1.pds.v1.client.socket.card.company.Company;
import fr.upec.episen.ing1.pds.v1.client.socket.card.person.Person;
import fr.upec.episen.ing1.pds.v1.client.socket.card.person.PersonSocket;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Objects;

public class AddCard extends CardHome implements ActionListener {
    private JButton submit;
    private JCheckBox statusBox;
    private JComboBox<String> comboBox;
    private JTextField firstNameField, lastNameField;
    public AddCard(Company company) {
        super(company);
        context.removeAll();
        context.setLayout(new BorderLayout());

        JLabel jLabel = new JLabel("Renseignez les informations ci-dessous", SwingConstants.CENTER);
        context.add(jLabel, BorderLayout.NORTH);

        JPanel addForm = new JPanel(new GridLayout(8, 2, 40, 40));

        comboBox = new JComboBox<>();
        comboBox.addItem("Administrateur");
        comboBox.addItem("Employé");
        comboBox.addItem("Visiteur");
        addForm.add(new JLabel("Type de badge"));
        addForm.add(comboBox);

        addForm.add(new JLabel("Nom du propriétaire"));
        lastNameField = new JTextField();
        addForm.add(lastNameField);

        addForm.add(new JLabel("Prénom du propriétaire"));
        firstNameField = new JTextField();
        addForm.add(firstNameField);

        statusBox = new JCheckBox("Actif");
        addForm.add(new JLabel("État de badge"));
        addForm.add(statusBox);

        JPanel submitPanel = new JPanel(new FlowLayout());
        submit = new JButton("Ajouter");
        submit.addActionListener(this);
        submitPanel.add(submit);
        addForm.add(submitPanel);
        context.add(addForm);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        super.actionPerformed(e);
        JButton eSource = (JButton) e.getSource();
        if(eSource == submit) {
            Badge badge = new Badge(
                    null,
                    statusBox.isSelected(),
                    Objects.requireNonNull(comboBox.getSelectedItem()).toString(),
                    null
            );
            Person person = new Person(
                    null,
                    firstNameField.getText(),
                    lastNameField.getText(),
                    statusBox.isSelected(),
                    currentCompany,
                    badge
            );
            PersonSocket.savePerson(person);
            JOptionPane.showMessageDialog(
                    null,
                    "Ajouter avec succès",
                    "Success",
                    JOptionPane.INFORMATION_MESSAGE
            );
        }
    }
}
