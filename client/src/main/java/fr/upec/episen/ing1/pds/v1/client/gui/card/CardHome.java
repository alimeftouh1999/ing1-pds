package fr.upec.episen.ing1.pds.v1.client.gui.card;

import fr.upec.episen.ing1.pds.v1.client.gui.AfterLogin;
import fr.upec.episen.ing1.pds.v1.client.gui.card.actions.SidebarAction;
import fr.upec.episen.ing1.pds.v1.client.socket.card.company.Company;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.util.Map;

public class CardHome extends AfterLogin {
    protected JPanel context = new JPanel();
    public CardHome(Company company) {
        super(company);
        this.content.removeAll();
        content.setLayout(new BorderLayout());
        JPanel sidebar = new JPanel(new GridLayout(4, 1, 60, 60));
        sidebar.setBackground(Color.gray);
        JButton addCard = new JButton("Ajouter un badge");
        transBtn(addCard);
        addCard.setActionCommand("ADD_CARD");
        SidebarAction sidebarAction = new SidebarAction(this);
        addCard.addActionListener(sidebarAction);
        sidebar.add(addCard);

        JButton modifyCard = new JButton("Modifier un badge");
        transBtn(modifyCard);
        modifyCard.setActionCommand("MODIFY_CARD");
        modifyCard.addActionListener(sidebarAction);
        sidebar.add(modifyCard);

        JButton deleteCard = new JButton("Supprimer un badge");
        transBtn(deleteCard);
        deleteCard.setActionCommand("DELETE_CARD");
        deleteCard.addActionListener(sidebarAction);
        sidebar.add(deleteCard);

        JButton testCard = new JButton("Tester un badge");
        transBtn(testCard);
        testCard.setActionCommand("TEST_CARD");
        testCard.addActionListener(sidebarAction);
        sidebar.add(testCard);

        content.add(sidebar, BorderLayout.WEST);
        content.add(context);
    }
    public static void transBtn(JButton modifyCard) {
        modifyCard.setOpaque(false);
        modifyCard.setContentAreaFilled(false);
        modifyCard.setForeground(Color.WHITE);
        modifyCard.setBorder(BorderFactory.createLineBorder(Color.WHITE));
    }
}
