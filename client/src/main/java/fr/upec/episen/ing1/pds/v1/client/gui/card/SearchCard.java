package fr.upec.episen.ing1.pds.v1.client.gui.card;

import fr.upec.episen.ing1.pds.v1.client.gui.UiUtils;
import fr.upec.episen.ing1.pds.v1.client.socket.card.company.Company;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.Map;

public class SearchCard extends CardHome {
    private final JButton click;
    private final JComboBox<Map<String, Object>> comboBox;
    private final JPanel showPanel;
    public SearchCard(Company company) {
        super(company);
        context.removeAll();

        showPanel = new JPanel();
        showPanel.setVisible(false);

        context.add(new JLabel(""));
        context.setLayout(new BorderLayout(40, 40));
        comboBox = UiUtils.getMapJComboBox(company);
        JPanel form = new JPanel(new BorderLayout(40, 40));
        form.add(new JLabel("Choisissez un personne"), BorderLayout.WEST);
        form.add(comboBox, BorderLayout.CENTER);
        click = new JButton("Click");
        click.addActionListener(this);
        form.add(click, BorderLayout.EAST);
        form.setBorder(new EmptyBorder(20, 5, 5, 5));
        context.add(form, BorderLayout.NORTH);
        context.add(showPanel, BorderLayout.CENTER);
    }
}
