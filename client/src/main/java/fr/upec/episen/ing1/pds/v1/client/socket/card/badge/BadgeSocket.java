package fr.upec.episen.ing1.pds.v1.client.socket.card.badge;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.upec.episen.ing1.pds.v1.client.socket.SocketUtil;
import fr.upec.episen.ing1.pds.v1.client.socket.card.company.Company;
import fr.upec.episen.ing1.pds.v1.client.socket.serialization.Request;
import fr.upec.episen.ing1.pds.v1.client.socket.serialization.Response;

import java.util.List;

public class BadgeSocket {
    private final static ObjectMapper MAPPER = new ObjectMapper();
    public static void saveBadge (Badge badge) {
        Request request = new Request("SAVE_BADGE");
        request.setData(MAPPER.convertValue(badge, JsonNode.class));
        Response t = SocketUtil.sendRequest(request);
        System.out.println(t.getMessage());
    }
}
