package fr.upec.episen.ing1.pds.v1.backend.card;

import fr.upec.episen.ing1.pds.v1.backend.card.building.Building;
import fr.upec.episen.ing1.pds.v1.backend.card.building.BuildingDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class FloorDAO implements DAO<Map<String, Object>> {
    private final Connection connection;
    private Long companyId;
    public FloorDAO(Connection connection) {
        this.connection = connection;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }
    @Override
    public List<Map<String, Object>> findAll() {
        BuildingDAO buildingDAO = new BuildingDAO(connection);
        buildingDAO.setCompanyId(companyId);
        String sql = "SELECT * FROM floors";
        List<Map<String, Object>> data = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                Building b = buildingDAO.findById(rs.getLong("building_number")).orElse(null);
                data.add(
                        Map.of(
                                "floor_id", rs.getLong("floor_id"),
                                "floor_number", rs.getString("floor_number"),
                                "building_name", b.getBuildingName(),
                                "building_number", rs.getLong("building_number")
                        )
                );
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }

    @Override
    public Optional<Map<String, Object>> findById(Long id) {
        String sql = "SELECT * FROM floors WHERE floor_id = ?";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1, id);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                return Optional.of(Map.of(
                        "floor_id", rs.getLong("floor_id"),
                        "floor_number", rs.getString("floor_number"),
                        "building_number", rs.getLong("building_number")
                ));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public Map<String, Object> save(Map<String, Object> map) {
        return null;
    }

    @Override
    public Map<String, Object> update(Map<String, Object> map, Long id) {
        return null;
    }

    @Override
    public void delete(Map<String, Object> map) {

    }

    public List<Map<String, Object>> findByBuilding(Long id) {
        String sql = "SELECT * FROM floors WHERE building_number = ?";
        List<Map<String, Object>> floorList = new ArrayList<>();
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1, id);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                 floorList.add(Map.of(
                        "floor_id", rs.getLong("floor_id"),
                        "floor_number", rs.getString("floor_number"),
                        "building_number", rs.getLong("building_number")
                ));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return floorList;
    }

    public void grantAccess(Long id, Long badgeId) {
        SpaceDao spaceDao = new SpaceDao(connection);
        List<Map<String, Object>> desks = spaceDao.spaceListByFloor(id);
        for (Map<String, Object> desk: desks) {
            spaceDao.grantAccess(Long.valueOf(String.valueOf(desk.get("space_id"))), badgeId);
        }
    }
    public void pullAccess(Long id, Long badgeId) {
        SpaceDao spaceDao = new SpaceDao(connection);
        List<Map<String, Object>> desks = spaceDao.spaceListByFloor(id);
        for (Map<String, Object> desk: desks) {
            spaceDao.pullAccess(Long.valueOf(String.valueOf(desk.get("space_id"))), badgeId);
        }
    }

    public boolean hasAccess(Long id, Long badgeId) {
        SpaceDao spaceDao = new SpaceDao(connection);
        List<Map<String, Object>> desks = spaceDao.spaceListByFloor(id);
        for (Map<String, Object> desk: desks) {
            boolean hasAccess = spaceDao.hasAccess(Long.valueOf(String.valueOf(desk.get("space_id"))), badgeId);
            if(hasAccess)
                return true;
        }
        return false;
    }
}
