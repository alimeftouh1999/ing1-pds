package fr.upec.episen.ing1.pds.v1.backend.tcp;

import fr.upec.episen.ing1.pds.v1.backend.pool.JDBCPoolConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;

public class ServerNetwork {
    private final static Logger LOGGER = LoggerFactory.getLogger(ServerNetwork.class);

    private final int port;
    private final JDBCPoolConnection poolConnection;

    public ServerNetwork(int port, JDBCPoolConnection poolConnection) {
        this.port = port;
        this.poolConnection = poolConnection;
    }

    public void serve() throws IOException {
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(port);
            LOGGER.info("The server socket has been launched");
            while (true) {
                Socket socket = serverSocket.accept();
                LOGGER.info("A new Client has been connected");
                Connection connection = poolConnection.getConnection();
                new Networkhandler(socket, connection, poolConnection);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(serverSocket != null) {
                serverSocket.close();
            }
        }
    }
}
