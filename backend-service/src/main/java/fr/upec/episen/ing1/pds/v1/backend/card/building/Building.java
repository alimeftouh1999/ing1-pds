package fr.upec.episen.ing1.pds.v1.backend.card.building;

import com.fasterxml.jackson.annotation.JsonSetter;
import fr.upec.episen.ing1.pds.v1.backend.card.company.Company;

public class Building {
    private Long id;
    private String buildingName;
    private String buildingAddress;
    private Company company;

    public Building(Long id, String buildingName, String buildingAddress, Company company) {
        this.id = id;
        this.buildingName = buildingName;
        this.buildingAddress = buildingAddress;
        this.company = company;
    }

    public Building() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBuildingName() {
        return buildingName;
    }

    @JsonSetter("building_name")
    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }

    public String getBuildingAddress() {
        return buildingAddress;
    }

    @JsonSetter("building_address")
    public void setBuildingAddress(String buildingAddress) {
        this.buildingAddress = buildingAddress;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}
