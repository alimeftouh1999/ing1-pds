package fr.upec.episen.ing1.pds.v1.backend.tcp.card.company;

import com.fasterxml.jackson.core.JsonProcessingException;
import fr.upec.episen.ing1.pds.v1.backend.card.DAO;
import fr.upec.episen.ing1.pds.v1.backend.card.company.Company;
import fr.upec.episen.ing1.pds.v1.backend.card.company.CompanyDAO;
import fr.upec.episen.ing1.pds.v1.backend.tcp.Request;
import fr.upec.episen.ing1.pds.v1.backend.tcp.card.Presentation;
import fr.upec.episen.ing1.pds.v1.backend.tcp.card.Utils;

import java.io.PrintWriter;
import java.sql.Connection;
import java.util.List;

public class CompanyTcp extends Presentation {

    public CompanyTcp(Connection connection, PrintWriter writer) {
        super(connection, writer);
    }

    @Override
    public void launch(Request request) throws JsonProcessingException {
        String event = request.getEvent();
        final DAO<Company> companyDAO = new CompanyDAO(connection);
        if(event.equals("COMPANY_LIST")) {
            List<Company> companies = companyDAO.findAll();
            writer.println(Utils.responseGenerator(companies));
        }
    }
}
