package fr.upec.episen.ing1.pds.v1.backend.card.badge;

import fr.upec.episen.ing1.pds.v1.backend.card.DAO;

import java.sql.*;
import java.sql.Date;
import java.util.*;

public class BadgeDAO implements DAO<Badge> {
    private final Connection connection;

    public BadgeDAO(Connection connection) {
        this.connection = connection;
    }

    @Override
    public List<Badge> findAll() {
        List<Badge> badges = new ArrayList<>();
        String sql = "SELECT * FROM badges ";
        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                Badge badge = new Badge(
                        rs.getLong("badge_id"),
                        rs.getBoolean("badge_active"),
                        rs.getString("badge_type"),
                        rs.getDate("badge_end_date")
                );
                if(Objects.nonNull(rs.getString("badge_serial"))) badge.setSerialNumber(UUID.fromString(rs.getString("badge_serial")));
                badges.add(badge);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return badges;
    }

    @Override
    public Optional<Badge> findById(Long id) {
        String sql = "SELECT * FROM badges WHERE badge_id = ?";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1, id);
            ResultSet rs = statement.executeQuery();
            if(rs.next()) {
                Badge badge = new Badge(
                        rs.getLong("badge_id"),
                        rs.getBoolean("badge_active"),
                        rs.getString("badge_type"),
                        rs.getDate("badge_end_date")
                );
                if(Objects.nonNull(rs.getString("badge_serial"))) badge.setSerialNumber(UUID.fromString(rs.getString("badge_serial")));
                return Optional.of(badge);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public Badge save(Badge badge) {
        String sql = "insert into badges (badge_active, badge_end_date, badge_type, badge_serial) values (?, ?, ?, ?)";
        try {
            PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setBoolean(1, badge.getActive());
            statement.setString(3, badge.getType());
            if(Objects.isNull(badge.getEndDate())) {
                statement.setDate(2, null);
            } else statement.setDate(2, new Date(badge.getEndDate().getTime()));
            statement.setString(4, UUID.randomUUID().toString());
            statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();

            if (rs.next()) {
                 badge.setId(rs.getLong(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return badge;
    }

    @Override
    public Badge update(Badge badge, Long id) {
        badge.setId(id);
        Badge badgeToFind = findById(id).orElse(badge);
        String sql = "update badges set badge_active = ?, badge_end_date = ?, badge_type = ? where badge_id = ?";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            if(Objects.isNull(badge.getActive())) badge.setActive(badgeToFind.getActive());
            if(Objects.isNull(badge.getEndDate())) badge.setEndDate(badgeToFind.getEndDate());
            if(Objects.isNull(badge.getType())) badge.setType(badgeToFind.getType());
            statement.setBoolean(1, badge.getActive());
            if(Objects.isNull(badge.getEndDate())) statement.setDate(2, null);
            else statement.setDate(2, new Date(badge.getEndDate().getTime()));
            statement.setString(3, badge.getType());
            statement.setLong(4, id);

            statement.executeUpdate();
            return badge;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return badge;
    }

    @Override
    public void delete(Badge badge) {
        String sql = "delete from badges where badge_id = ?";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1, badge.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
