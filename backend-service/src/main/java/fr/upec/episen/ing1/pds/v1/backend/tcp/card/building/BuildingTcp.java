package fr.upec.episen.ing1.pds.v1.backend.tcp.card.building;

import com.fasterxml.jackson.core.JsonProcessingException;
import fr.upec.episen.ing1.pds.v1.backend.card.FloorDAO;
import fr.upec.episen.ing1.pds.v1.backend.card.SpaceDao;
import fr.upec.episen.ing1.pds.v1.backend.card.building.Building;
import fr.upec.episen.ing1.pds.v1.backend.card.building.BuildingDAO;
import fr.upec.episen.ing1.pds.v1.backend.tcp.Request;
import fr.upec.episen.ing1.pds.v1.backend.tcp.card.Presentation;
import fr.upec.episen.ing1.pds.v1.backend.tcp.card.Utils;

import java.io.PrintWriter;
import java.sql.Connection;
import java.util.*;
import java.util.stream.Collectors;

public class BuildingTcp extends Presentation {
    public BuildingTcp(Connection connection, PrintWriter writer) {
        super(connection, writer);
    }

    @Override
    public void launch(Request request) throws JsonProcessingException {
        String event = request.getEvent();
        BuildingDAO dao = new BuildingDAO(connection);
        FloorDAO floorDAO = new FloorDAO(connection);
        SpaceDao spaceDao = new SpaceDao(connection);
        if(Objects.equals(event, "BUILDING_LIST")) {
            dao.setCompanyId(request.getData().get("'company_id'").asLong());
            String jsonResponse = Utils.responseGenerator(dao.findAll());
            writer.println(jsonResponse);
        } else if(Objects.equals(event, "BUILDING_BY_ID")) {
            Long id = request.getData().get("'id'").asLong();
            Optional<Building> maybeBuilding = dao.findById(id);
            if(maybeBuilding.isPresent()) {
                String jsonResponse = Utils.responseGenerator(maybeBuilding.get());
                writer.println(jsonResponse);
            } else {
                writer.println(Utils.responseGenerator(new Building()));
            }
        } else if(Objects.equals(event, "SAVE_BUILDING")) {
            dao.setCompanyId(request.getData().get("company_id").asLong());
            Building building = MAPPER.treeToValue(request.getData().get("building"), Building.class);
            String jsonResponse = Utils.responseGenerator(dao.save(building));
            writer.println(jsonResponse);
        } else if(Objects.equals(event, "FLOOR_BY_BUILDING")) {
            Long buildingId = request.getData().get("'building_id'").asLong();
            String jsonResponse = Utils.responseGenerator(floorDAO.findByBuilding(buildingId));
            writer.println(jsonResponse);
        } else if(Objects.equals(event, "SPACES_BY_FLOOR")) {
            Long floorId = request.getData().get("'floor_id'").asLong();
            String json = Utils.responseGenerator(spaceDao.spaceListByFloor(floorId));
            writer.println(json);
        } else if(Objects.equals(event, "GRANT_ACCESS_DESK")) {
            Long spaceId = request.getData().get("space_id").asLong();
            Long badgeId = request.getData().get("badge_id").asLong();
            spaceDao.grantAccess(spaceId, badgeId);
            String json = Utils.responseGenerator(Map.of());
            writer.println(json);
        } else if(Objects.equals(event, "PULL_ACCESS_DESK")) {
            Long spaceId = request.getData().get("space_id").asLong();
            Long badgeId = request.getData().get("badge_id").asLong();
            spaceDao.pullAccess(spaceId, badgeId);
            String json = Utils.responseGenerator(Map.of());
            writer.println(json);
        } else if(Objects.equals(event,"GRANT_ACCESS_FLOOR")) {
            Long floorId = request.getData().get("floor_id").asLong();
            Long badgeId = request.getData().get("badge_id").asLong();
            floorDAO.grantAccess(floorId, badgeId);
            String json = Utils.responseGenerator(Map.of());
            writer.println(json);
        } else if(Objects.equals(event, "PULL_ACCESS_FLOOR")) {
            Long floorId = request.getData().get("floor_id").asLong();
            Long badgeId = request.getData().get("badge_id").asLong();
            floorDAO.pullAccess(floorId, badgeId);
            String json = Utils.responseGenerator(Map.of());
            writer.println(json);
        } else if(Objects.equals(event, "GRANT_ACCESS_BUILDING")) {
            Long buildingId = request.getData().get("building_id").asLong();
            Long badgeId = request.getData().get("badge_id").asLong();
            dao.grantAccess(buildingId, badgeId);
            String json = Utils.responseGenerator(Map.of());
            writer.println(json);
        } else if(Objects.equals(event, "PULL_ACCESS_BUILDING")) {
            Long buildingId = request.getData().get("building_id").asLong();
            Long badgeId = request.getData().get("badge_id").asLong();
            dao.pullAccess(buildingId, badgeId);
            String json = Utils.responseGenerator(Map.of());
            writer.println(json);
        } else if(Objects.equals(event, "HAS_ACCESS_BUILDING")) {
            Long buildingId = request.getData().get("building_id").asLong();
            Long badgeId = request.getData().get("badge_id").asLong();
            String json = Utils.responseGenerator(dao.hasAccess(buildingId, badgeId));
            writer.println(json);
        } else if(Objects.equals(event, "HAS_ACCESS_FLOOR")) {
            Long floorId = request.getData().get("floor_id").asLong();
            Long badgeId = request.getData().get("badge_id").asLong();
            String json = Utils.responseGenerator(floorDAO.hasAccess(floorId, badgeId));
            writer.println(json);
        }
        else if(Objects.equals(event, "HAS_ACCESS_SPACE")) {
            Long spaceId = request.getData().get("space_id").asLong();
            Long badgeId = request.getData().get("badge_id").asLong();
            String json = Utils.responseGenerator(spaceDao.hasAccess(spaceId, badgeId));
            System.out.println(json);
            writer.println(json);
        }
        else if(Objects.equals(event, "ACCESS_ITEMS")) {
            Map data = new HashMap();
            Long badgeId = request.getData().get("badge_id").asLong();
            dao.setCompanyId(request.getData().get("'company_id'").asLong());
            List<Building> buildings = dao.findAll().stream().filter(b -> dao.hasAccess(b.getId(), badgeId)).collect(Collectors.toList());
            data.put("buildings", buildings);
            floorDAO.setCompanyId(request.getData().get("'company_id'").asLong());
            List<Map<String, Object>> floors = floorDAO.findAll().stream()
                    .filter(f -> floorDAO.hasAccess((Long) f.get("floor_id"), badgeId))
                    .collect(Collectors.toList());
            data.put("floors", floors);

            List<Map<String, Object>> spaces = floorDAO.findAll().stream()
                    .map(f -> spaceDao.spaceListByFloor((Long) f.get("floor_id")))
                    .reduce((a, c) -> {
                        a.addAll(c);
                        return a;
                    }).orElse(null);
            if(Objects.nonNull(spaces)) {
                List<Map<String, Object>> spacesAccess = spaces.stream()
                        .filter(s -> spaceDao.hasAccess((Long) s.get("space_id"), badgeId))
                        .collect(Collectors.toList());
                data.put("spaces", spacesAccess);
            }

            String json = Utils.responseGenerator(data);
            System.out.println(json);
            writer.println(json);
        }
    }
}
