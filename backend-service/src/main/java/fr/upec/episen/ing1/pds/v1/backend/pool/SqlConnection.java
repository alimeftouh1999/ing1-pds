package fr.upec.episen.ing1.pds.v1.backend.pool;

import java.sql.Connection;
import java.sql.DriverManager;

public class SqlConnection {
    private final static String URL = "jdbc:postgresql://172.31.250.3/alidb";
    private final static String USER = "ali";
    private final static String PASS = "ali";

    public static Connection getConnection() {
        try {
            return DriverManager.getConnection(URL, USER, PASS);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }
}
