package fr.upec.episen.ing1.pds.v1.backend.card.person;

import fr.upec.episen.ing1.pds.v1.backend.card.badge.Badge;
import fr.upec.episen.ing1.pds.v1.backend.card.company.Company;

public class Person {
    private Long id;
    private String firstName;
    private String lastName;
    private Boolean isVisitor;
    private Company company;
    private Badge badge;

    public Person() {
    }

    public Person(Long id, String firstName, String lastName, Boolean isVisitor, Company company, Badge badge) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.isVisitor = isVisitor;
        this.company = company;
        this.badge = badge;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Boolean getVisitor() {
        return isVisitor;
    }

    public void setVisitor(Boolean visitor) {
        isVisitor = visitor;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Badge getBadge() {
        return badge;
    }

    public void setBadge(Badge badge) {
        this.badge = badge;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", isVisitor=" + isVisitor +
                ", company=" + company +
                ", badge=" + badge +
                '}';
    }
}
