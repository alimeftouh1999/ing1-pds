package fr.upec.episen.ing1.pds.v1.backend.tcp.card;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Map;
import java.util.Objects;

public class Utils {
    private final static ObjectMapper MAPPER = new ObjectMapper();
    public static String responseGenerator(Object data) throws JsonProcessingException {
        return MAPPER.writeValueAsString(Map.of("success", "true", "message", data));
    }
}
