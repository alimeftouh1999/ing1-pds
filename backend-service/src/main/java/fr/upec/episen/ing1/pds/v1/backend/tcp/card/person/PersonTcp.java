package fr.upec.episen.ing1.pds.v1.backend.tcp.card.person;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.upec.episen.ing1.pds.v1.backend.card.person.Person;
import fr.upec.episen.ing1.pds.v1.backend.card.person.PersonDAO;
import fr.upec.episen.ing1.pds.v1.backend.tcp.Request;
import fr.upec.episen.ing1.pds.v1.backend.tcp.card.Presentation;
import fr.upec.episen.ing1.pds.v1.backend.tcp.card.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.PrintWriter;
import java.sql.Connection;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

public class PersonTcp extends Presentation {


    public PersonTcp(Connection connection, PrintWriter writer) {
        super(connection, writer);
    }

    @Override
    public void launch(Request request) throws JsonProcessingException {
        String event = request.getEvent();
        PersonDAO dao = new PersonDAO(connection);
        LOGGER.info("RECEIVING : {}", request.getData());
        if(Objects.equals(event, "PERSONS_LIST")) {
            dao.setCompanyId(request.getData().get("'company_id'").asLong());
            String jsonResponse = Utils.responseGenerator(dao.findAll());
            writer.println(jsonResponse);
        } else if (Objects.equals(event, "PERSON_BY_ID")) {
            Long id = request.getData().get("'person_id'").asLong();
            Optional<Person> optionalPerson = dao.findById(id);
            String jsonResponse;
            if(optionalPerson.isPresent()) jsonResponse = Utils.responseGenerator(optionalPerson.get());
            else jsonResponse = Utils.responseGenerator(Map.of());
            writer.println(jsonResponse);
        } else if (Objects.equals(event, "SAVE_PERSON")) {
            Person person = MAPPER.treeToValue(request.getData(), Person.class);
            Person newPerson = dao.save(person);
            String json = Utils.responseGenerator(newPerson);
            writer.println(json);
        } else if(Objects.equals(event, "DELETE_PERSON")) {
            Person person = MAPPER.treeToValue(request.getData(), Person.class);
            dao.delete(person);
            String json = Utils.responseGenerator(person);
            writer.println(json);
        } else if(Objects.equals(event, "UPDATE_PERSON")) {
            JsonNode data = request.getData();
            Long id = data.get("'person_id'").asLong();
            Person p = MAPPER.readValue(data.get("person_data").asText(), Person.class);
            LOGGER.info("P deserialized : {}", p);
            Person p2 = dao.update(p, id);
            writer.println(Utils.responseGenerator(p2));
        }
    }
}
