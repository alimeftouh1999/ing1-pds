package fr.upec.episen.ing1.pds.v1.backend.card;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

public interface DAO<E> extends Serializable {
    List<E> findAll();
    Optional<E> findById(Long id);
    E save(E e);
    E update(E e, Long id);
    void delete(E e);
}
