package fr.upec.episen.ing1.pds.v1.backend.card;

import fr.upec.episen.ing1.pds.v1.backend.card.badge.Badge;
import fr.upec.episen.ing1.pds.v1.backend.card.badge.BadgeDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;

public class SpaceDao {
    private final Connection connection;

    public SpaceDao(Connection connection) {
        this.connection = connection;
    }

    public List<Map<String, Object>> spaceListByFloor(Long id) {
        String sql = "SELECT * FROM spaces WHERE floor_id = ?";
        List<Map<String, Object>> spaces = new ArrayList<>();
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1, id);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                spaces.add(Map.of(
                        "space_id", rs.getLong("space_id"),
                        "space_type", rs.getString("space_type"),
                        "space_name", rs.getString("space_name"),
                        "floor_id", rs.getLong("floor_id")
                ));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return spaces;
    }

    public void grantAccess(Long id, Long badgeId) {
        String sql = "insert into permissions (space_id, badge_id) values (?, ?)";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1, id);
            statement.setLong(2, badgeId);
            statement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void pullAccess(Long id, Long badgeId) {
        String sql = "delete from permissions where space_id = ? and badge_id = ?";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1, id);
            statement.setLong(2, badgeId);
            statement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean hasAccess(Long id, Long badgeId) {
        BadgeDAO badgeDAO = new BadgeDAO(connection);
        Badge badge = badgeDAO.findById(badgeId).orElse(null);
        if(Objects.nonNull(badge)) {
            if(! badge.getActive()) {
                return false;
            } else if(Objects.equals(badge.getType(), "Administrateur"))  {
                return true;
            }else if(Objects.nonNull(badge.getEndDate())) {
                if(badge.getEndDate().compareTo(new Date()) < 0) {
                    return false;
                }
            }
            String sql = "SELECT *  from permissions where space_id = ? and badge_id = ?";
            try {
                PreparedStatement statement = connection.prepareStatement(sql);
                statement.setLong(1, id);
                statement.setLong(2, badgeId);
                ResultSet rs = statement.executeQuery();
                return rs.next();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }
}
