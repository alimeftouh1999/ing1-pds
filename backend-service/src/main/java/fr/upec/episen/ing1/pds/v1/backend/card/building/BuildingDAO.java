package fr.upec.episen.ing1.pds.v1.backend.card.building;

import fr.upec.episen.ing1.pds.v1.backend.card.DAO;
import fr.upec.episen.ing1.pds.v1.backend.card.FloorDAO;
import fr.upec.episen.ing1.pds.v1.backend.card.SpaceDao;
import fr.upec.episen.ing1.pds.v1.backend.card.badge.Badge;
import fr.upec.episen.ing1.pds.v1.backend.card.company.Company;
import fr.upec.episen.ing1.pds.v1.backend.card.company.CompanyDAO;
import fr.upec.episen.ing1.pds.v1.backend.card.person.Person;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class BuildingDAO implements DAO<Building> {
    private final Connection connection;
    private final DAO<Company> companyDAO;

    private Long companyId;

    public BuildingDAO(Connection connection) {
        this.connection = connection;
        this.companyDAO = new CompanyDAO(connection);
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    @Override
    public List<Building> findAll() {
        List<Building> buildings = new ArrayList<>();
        String sql = "SELECT * FROM buildings WHERE company_number = " + companyId;
        Company company = companyDAO.findById(companyId).orElse(null);
        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                Building b = new Building(
                        rs.getLong("building_number"),
                        rs.getString("building_name"),
                        rs.getString("building_address"),
                        company
                );
                buildings.add(b);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return buildings;
    }

    @Override
    public Optional<Building> findById(Long id) {
        String sql = "SELECT * FROM buildings WHERE building_number = ?";
        Company company = companyDAO.findById(companyId).orElse(null);
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1, id);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                Building b = new Building(
                        rs.getLong("building_number"),
                        rs.getString("building_name"),
                        rs.getString("building_address"),
                        company
                );
                return Optional.of(b);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public Building save(Building building) {
        String sql = "insert into buildings (building_name, building_address, company_number) values (?, ?, ?)";
        try {
            PreparedStatement statement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);

            statement.setString(1, building.getBuildingName());
            statement.setString(2, building.getBuildingAddress());
            statement.setLong(3, companyId);

            statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();
            if(rs.next())
                building.setId(rs.getLong("building_number"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return building;
    }

    @Override
    public Building update(Building building, Long id) {
        throw new IllegalStateException("Not Impl yet !");
    }

    @Override
    public void delete(Building building) {
        throw new IllegalStateException("Not Impl yet !");
    }

    public void grantAccess(Long id, Long badgeId) {
        FloorDAO floorDAO = new FloorDAO(connection);
        List<Map<String, Object>> floors = floorDAO.findByBuilding(id);
        for (Map<String, Object> floor: floors) {
            floorDAO.grantAccess(Long.valueOf(String.valueOf(floor.get("floor_id"))), badgeId);
        }
    }

    public void pullAccess(Long id, Long badgeId) {
        FloorDAO floorDAO = new FloorDAO(connection);
        List<Map<String, Object>> floors = floorDAO.findByBuilding(id);
        for (Map<String, Object> floor: floors) {
            floorDAO.pullAccess(Long.valueOf(String.valueOf(floor.get("floor_id"))), badgeId);
        }
    }

    public boolean hasAccess(Long id, Long badgeId) {
        FloorDAO floorDAO = new FloorDAO(connection);
        List<Map<String, Object>> floors = floorDAO.findByBuilding(id);
        for (Map<String, Object> floor: floors) {
            boolean hasAccess = floorDAO.hasAccess(Long.valueOf(String.valueOf(floor.get("floor_id"))), badgeId);
            if(hasAccess) return true;
        }
        return false;
    }
}
