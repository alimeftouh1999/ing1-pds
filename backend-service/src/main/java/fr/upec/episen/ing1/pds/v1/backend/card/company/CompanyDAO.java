package fr.upec.episen.ing1.pds.v1.backend.card.company;

import fr.upec.episen.ing1.pds.v1.backend.card.DAO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class CompanyDAO implements DAO<Company> {
    private final Connection connection;

    public CompanyDAO(Connection connection) {
        this.connection = connection;
    }

    @Override
    public List<Company> findAll() {
        List<Company> companies = new ArrayList<>();
        String sql = "SELECT * FROM companies";
        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                companies.add(new Company(
                        rs.getLong("company_number"),
                        rs.getString("company_name"),
                        rs.getString("company_address")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return companies;
    }

    @Override
    public Optional<Company> findById(Long id) {
        String sql = "SELECT * FROM companies WHERE company_number = ?";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1, id);
            ResultSet rs = statement.executeQuery();
            if(rs.next()) {
                Company company = new Company(
                        rs.getLong("company_number"),
                        rs.getString("company_name"),
                        rs.getString("company_address")
                );
                return Optional.of(company);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public Company save(Company company) {
        String sql = "insert into companies (company_name, company_address)values (?, ?)";
        try {
            PreparedStatement statement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            statement.setString(1, company.getName());
            statement.setString(2, company.getAddress());

            ResultSet rs = statement.getGeneratedKeys();
            if(rs.next())
                company.setId(rs.getLong("company_number"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return company;
    }

    @Override
    public Company update(Company company, Long id) {
        company.setId(id);
        Company companyToFind = findById(id).orElse(company);
        String sql = "update companies set company_name = ?, company_address = ? where company_number = ?";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            if(Objects.isNull(company.getAddress())) company.setAddress(companyToFind.getAddress());
            if(Objects.isNull(company.getName())) company.setName(companyToFind.getName());

            statement.setString(1, company.getName());
            statement.setString(2, company.getAddress());
            statement.setLong(3, company.getId());

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return company;
    }

    @Override
    public void delete(Company company) {
        String sql = "delete from companies where company_number = ?";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1, company.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
