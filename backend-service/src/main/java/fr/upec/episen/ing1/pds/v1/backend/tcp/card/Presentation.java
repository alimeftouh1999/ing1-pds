package fr.upec.episen.ing1.pds.v1.backend.tcp.card;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.upec.episen.ing1.pds.v1.backend.tcp.Request;
import fr.upec.episen.ing1.pds.v1.backend.tcp.card.badge.BadgeTcp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.PrintWriter;
import java.sql.Connection;

abstract public class Presentation {
     protected final  Logger LOGGER = LoggerFactory.getLogger(this.getClass());
     protected final static ObjectMapper MAPPER = new ObjectMapper();

     protected final Connection connection;
     protected final PrintWriter writer;

     protected Presentation(Connection connection, PrintWriter writer) {
          this.connection = connection;
          this.writer = writer;
     }


     public abstract void launch(Request request) throws JsonProcessingException;
}
