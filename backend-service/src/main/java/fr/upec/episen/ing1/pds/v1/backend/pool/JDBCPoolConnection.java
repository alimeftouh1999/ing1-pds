package fr.upec.episen.ing1.pds.v1.backend.pool;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.util.Stack;

public class JDBCPoolConnection {
    private final static Logger LOGGER = LoggerFactory.getLogger(JDBCPoolConnection.class);
    private final static Stack<Connection> pool = new Stack<>();
    private final int nbConnection;

    public JDBCPoolConnection(int nbConnection) {
        this.nbConnection = nbConnection;
        this.initPool();
    }

    protected void initPool() {
        for (int i = 0; i < nbConnection; i++) {
            pool.push(SqlConnection.getConnection());
        }
    }
    public Connection getConnection() {
        if(pool.size() > 0) {
            return pool.pop();
        } else {
            try {
                Thread.sleep(120);
                return pool.pop();
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                return null;
            }
        }
    }
    public void returnConnection(Connection connection) {
        if(pool.size() < nbConnection)
            pool.add(0,connection);
    }
}
