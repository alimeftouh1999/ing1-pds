package fr.upec.episen.ing1.pds.v1.backend.tcp.card.badge;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.upec.episen.ing1.pds.v1.backend.card.DAO;
import fr.upec.episen.ing1.pds.v1.backend.card.badge.Badge;
import fr.upec.episen.ing1.pds.v1.backend.card.badge.BadgeDAO;
import fr.upec.episen.ing1.pds.v1.backend.tcp.Request;
import fr.upec.episen.ing1.pds.v1.backend.tcp.card.Presentation;
import fr.upec.episen.ing1.pds.v1.backend.tcp.card.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.PrintWriter;
import java.sql.Connection;

public class BadgeTcp extends Presentation {

    public BadgeTcp(Connection connection, PrintWriter writer) {
        super(connection, writer);
    }

    @Override
    public void launch(Request request) throws JsonProcessingException {
        String r = request.getEvent();
        if(r.equals("SAVE_BADGE")) {
            DAO<Badge> badgeDAO = new BadgeDAO(connection);
            LOGGER.info("Receiving : {}", request.getData());
            Badge badge = MAPPER.treeToValue(request.getData(), Badge.class);
            Badge savedBadge = badgeDAO.save(badge);
            String json = Utils.responseGenerator(savedBadge);
            LOGGER.info("Data : {}",json);
            writer.println(json);
        }
    }
}
