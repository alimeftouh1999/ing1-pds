package fr.upec.episen.ing1.pds.v1.backend;

import fr.upec.episen.ing1.pds.v1.backend.card.FloorDAO;
import fr.upec.episen.ing1.pds.v1.backend.pool.JDBCPoolConnection;
import fr.upec.episen.ing1.pds.v1.backend.tcp.ServerNetwork;
import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BackendService {
    private final static Logger LOGGER = LoggerFactory.getLogger(BackendService.class);
    public static void main(String[] args) throws Exception {
        Options options = new Options();

        Option testModeOpt = Option.builder().longOpt("testMode").build();
        options.addOption(testModeOpt);

        Option maxConnectionOpt = Option.builder().longOpt("maxConnection").hasArg().build();
        options.addOption(maxConnectionOpt);

        Option portOpt = Option.builder().longOpt("port").hasArg().build();
        options.addOption(portOpt);

        DefaultParser defaultParser = new DefaultParser();
        CommandLine cli = defaultParser.parse(options, args);

        boolean testMode = cli.hasOption(testModeOpt.getLongOpt());
        int maxConnection = 10;
        if(cli.hasOption("maxConnection")) {
            maxConnection = Integer.parseInt(cli.getOptionValue("maxConnection"));
        }

        int port = 61100;
        if(cli.hasOption("port")) {
            port = Integer.parseInt(cli.getOptionValue("port"));
        }

        LOGGER.info("The backend service started with {}, connectionx = {}", testMode, maxConnection);

        if(testMode) {
            JDBCPoolConnection poolConnection = new JDBCPoolConnection(maxConnection);
            ServerNetwork serverNetwork = new ServerNetwork(port, poolConnection);
            serverNetwork.serve();
        }

    }
}
