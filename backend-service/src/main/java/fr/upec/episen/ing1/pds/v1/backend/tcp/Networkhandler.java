package fr.upec.episen.ing1.pds.v1.backend.tcp;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.upec.episen.ing1.pds.v1.backend.pool.JDBCPoolConnection;
import fr.upec.episen.ing1.pds.v1.backend.tcp.card.badge.BadgeTcp;
import fr.upec.episen.ing1.pds.v1.backend.tcp.card.building.BuildingTcp;
import fr.upec.episen.ing1.pds.v1.backend.tcp.card.company.CompanyTcp;
import fr.upec.episen.ing1.pds.v1.backend.tcp.card.person.PersonTcp;
import fr.upec.episen.ing1.pds.v1.backend.tcp.card.Presentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.Connection;

public class Networkhandler implements Runnable {
    private final static ObjectMapper MAPPER = new ObjectMapper();
    private final static Logger LOGGER = LoggerFactory.getLogger(Networkhandler.class);
    private final Socket socket;
    private final Connection connection;
    private final Thread thread;
    private final JDBCPoolConnection poolConnection;
    public Networkhandler(Socket socket, Connection connection, final JDBCPoolConnection poolConnection) {
        this.socket = socket;
        this.connection = connection;
        this.poolConnection = poolConnection;
        this.thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() {
       try {
           PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
           BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
           String msg;
           while ((msg = reader.readLine()) != null) {
               Request received = MAPPER.readValue(msg, Request.class);
                LOGGER.info("We receive {}", received.getEvent());
               Presentation person = new PersonTcp(connection, writer);
               Presentation company = new CompanyTcp(connection, writer);
               Presentation badge = new BadgeTcp(connection, writer);
               Presentation building = new BuildingTcp(connection, writer);
               person.launch(received);
               company.launch(received);
               badge.launch(received);
               building.launch(received);
           }
           poolConnection.returnConnection(connection);
           thread.join();
       } catch (Exception e) {
           LOGGER.error(e.getMessage(), e);
       }
    }
}
