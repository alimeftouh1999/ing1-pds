package fr.upec.episen.ing1.pds.v1.backend.card.person;

import fr.upec.episen.ing1.pds.v1.backend.card.DAO;
import fr.upec.episen.ing1.pds.v1.backend.card.badge.Badge;
import fr.upec.episen.ing1.pds.v1.backend.card.badge.BadgeDAO;
import fr.upec.episen.ing1.pds.v1.backend.card.company.Company;
import fr.upec.episen.ing1.pds.v1.backend.card.company.CompanyDAO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class PersonDAO implements DAO<Person> {
    private final Connection connection;
    private final CompanyDAO companyDAO;
    private final BadgeDAO badgeDAO;

    private Long companyId;

    public PersonDAO(Connection connection) {
        this.connection = connection;
        this.companyDAO = new CompanyDAO(connection);
        this.badgeDAO = new BadgeDAO(connection);
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    @Override
    public List<Person> findAll() {
        List<Person> personList = new ArrayList<>();
        String sql = "SELECT * FROM persons WHERE person_company_number = " + companyId;
        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                Person p = new Person(
                        rs.getLong("person_id"),
                        rs.getString("person_first_name"),
                        rs.getString("person_last_name"),
                        rs.getBoolean("person_is_visitor"),
                        companyDAO.findById(rs.getLong("person_company_number")).orElse(null),
                        badgeDAO.findById(rs.getLong("person_badge")).orElse(null)
                );
                personList.add(p);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return personList;
    }

    @Override
    public Optional<Person> findById(Long id) {
        String sql = "SELECT * FROM persons WHERE person_id = ?";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1, id);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                Person p = new Person(
                        rs.getLong("person_id"),
                        rs.getString("person_first_name"),
                        rs.getString("person_last_name"),
                        rs.getBoolean("person_is_visitor"),
                        companyDAO.findById(rs.getLong("person_company_number")).orElse(null),
                        badgeDAO.findById(rs.getLong("person_badge")).orElse(null)
                );
                return Optional.of(p);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public Person save(Person person) {
        String sql = "insert into persons (person_first_name, person_last_name, person_is_visitor, person_company_number,person_badge)values (?, ?, ?, ?, ?)";
        try {
            PreparedStatement statement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            Company company = companyDAO.save(person.getCompany());
            Badge badge = badgeDAO.save(person.getBadge());

            statement.setString(1, person.getFirstName());
            statement.setString(2, person.getLastName());
            statement.setBoolean(3, person.getVisitor());
            statement.setLong(4, company.getId());
            statement.setLong(5, badge.getId());

            statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();
            if(rs.next())
                person.setId(rs.getLong("person_id"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return person;
    }

    @Override
    public Person update(Person person, Long id) {
        person.setId(id);
        Person person1 = findById(id).orElse(null);
        String sql = "update persons set person_first_name = ?, person_last_name = ?, person_is_visitor = ? where person_id = ?";
        try {
            assert person1 != null;
            if(Objects.isNull(person.getFirstName())) person.setFirstName(person1.getFirstName());
            if(Objects.isNull(person.getLastName())) person.setLastName(person1.getLastName());
            if(Objects.isNull(person.getVisitor())) person.setVisitor(person1.getVisitor());

            PreparedStatement statement = connection.prepareStatement(sql);

            statement.setString(1, person.getFirstName());
            statement.setString(2, person.getLastName());
            statement.setBoolean(3, person.getVisitor());
            statement.setLong(4, id);

            badgeDAO.update(person.getBadge(), person1.getBadge().getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return person;
    }

    @Override
    public void delete(Person person) {
        String sql = "delete from persons where person_id = ?";
        try {
            person = this.findById(person.getId()).orElse(null);
            if(Objects.nonNull(person)) {
                badgeDAO.delete(person.getBadge());
                PreparedStatement statement = connection.prepareStatement(sql);
                statement.setLong(1, person.getId());
                statement.executeUpdate();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
