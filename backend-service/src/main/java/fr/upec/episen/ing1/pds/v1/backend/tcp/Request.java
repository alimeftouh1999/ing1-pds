package fr.upec.episen.ing1.pds.v1.backend.tcp;

import com.fasterxml.jackson.databind.JsonNode;

public class Request {
    private String event;
    private JsonNode data;

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public JsonNode getData() {
        return data;
    }

    public void setData(JsonNode data) {
        this.data = data;
    }
}
