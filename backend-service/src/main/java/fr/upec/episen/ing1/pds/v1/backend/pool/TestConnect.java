package fr.upec.episen.ing1.pds.v1.backend.pool;

import fr.upec.episen.ing1.pds.v1.backend.card.DAO;
import fr.upec.episen.ing1.pds.v1.backend.card.badge.Badge;
import fr.upec.episen.ing1.pds.v1.backend.card.badge.BadgeDAO;

import java.sql.Connection;

public class TestConnect {
    public static void main(String[] args) {
        JDBCPoolConnection poolConnection = new JDBCPoolConnection(4);
        Connection connection = poolConnection.getConnection();
        DAO<Badge> badgeDAO = new BadgeDAO(connection);
        Badge badge = new Badge(null, false, "ACCESS_LIMITED", null);
        System.out.println(badgeDAO.update(badge, 3L));
    }
}
